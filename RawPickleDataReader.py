import tkinter as tk
from tkinter import filedialog
import numpy as np
import matplotlib.pyplot as plt
import _pickle as pickle
import datetime
from collections import defaultdict


def plot_result(phasez,absz, frequency):
    """This function plots the results in 2 plots, the real and imaginary
    impedance, against the frequency. The graph will be autoranged on the
    X and Y axis"""
    _, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(frequency, phasez)
    ax1.grid(True)
    ax1.set_ylabel('phasez')
    ax1.autoscale()
    ax2.plot(frequency, absz)
    ax2.grid(True)
    ax2.set_ylabel('absz')
    ax2.autoscale()
    ax2.set_xlabel('Frequency ($Hz$)')
    plt.draw()
    plt.show()
    print("test2")
    return

def plot_big_result(realz, phasez, frequency):
    """This function plots the results in 2 plots, the real and imaginary
    impedance, against the frequency. The graph will be autoranged on the
    X and Y axis"""
    _, (ax1, ax2) = plt.subplots(2, 1)
    for index in range(0,len(frequency)):
        ax2.plot(frequency[index], realz[index])
        ax1.plot(frequency[index], phasez[index])
    ax1.grid(True)
    ax1.set_ylabel('phase')
    ax1.autoscale()
    ax2.grid(True)
    ax2.set_ylabel('absz')
    ax2.autoscale()
    ax2.set_xlabel('Frequency ($Hz$)')
    plt.draw()
    plt.show()
    print("test")
    return




def create_file(name, header_string,folder):
    with open(folder + '/'+ name + '.txt', 'w' , encoding="utf8") as f:
        f.write("\ntest started at " + str(datetime.datetime.now()))
        f.write("\n"+header_string)

def save_obj3(obj, name,folder ):
    dic = defaultdict()
    dic = obj
    with open(folder + '/'+ name + '.txt', 'w' , encoding="utf8") as f:
        for keys in dic:
            f.write("\n")
            for keys2 in (dic[keys]):
                f.write("{ " + keys2 + "\t: " + str((dic[keys])[keys2]) + " }\t")

def append_file(obj, name,folder,doheader):
    with open(folder + '/' + name + '.txt', 'a' , encoding="utf8") as f:
            f.write("\n")
            if doheader:
                for keys2 in obj:
                    f.write(keys2+";")
                f.write("\n")
            for keys2 in obj:
                f.write(str(obj[keys2])+";")

def append_file2(obj, name,folder):
    dic = obj
    with open(folder + '/' + name + '.txt', 'a' , encoding="utf8") as f:
            f.write("\n")
            for keys2 in dic:
                f.write("{ " + keys2 + "\t: " + str(dic[keys2]) + " }\t")


def save_obj2(obj, name,folder):
    dic = obj
    with open(folder + '/'+ name + '.txt', 'w' , encoding="utf8") as f:
        for keys in dic:
            f.write("\n"+str(dic[keys]))

def save_obj(obj, name,folder):
    with open(folder + '/'+ name + '.txt', 'wb+') as f:
        pickle.dump(obj, f, 3)

def load_obj(path):
    with open(path,'rb') as f:
        return pickle.load(f)

def get_path_through_window():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path

def dataread(data,a):
    succes = True
    #data object is a dictionary
    try:
        imps_data_dict = ((list(data[a]['/dev3481/imps/0/sample']))[0])[0]
    except KeyError:
        succes = False
    absz = imps_data_dict['absz']
    freq = imps_data_dict['frequency']
    phase = imps_data_dict['phasez']
    return (absz, phase, freq, succes)

def rawData_to_plot():
    path = get_path_through_window()
    print(path)
    flag = False
    dic = load_obj(path)
    try:
        set= int(input("Object has " + str(len(dic)) + " data sets " + "please enter the nimber of the set you want to display or enter 'a' to display all data:"))
    except ValueError:
        flag = True
        print("not a number")

    if(flag):
        #this is all data
        a1 = list()
        a2 = list()
        a3 = list()
        a4 = list()
        a5 = list()
        a6 = list()

        for key in dic:
            #we delete the first element in the array because it seems to be incorrect almost always
            imps_data_dict = ((list(dic[key]['/dev3481/imps/0/sample']))[0])[0]
            demod_data_dict = ((list(dic[key]['/dev3481/demods/0/sample']))[0])[0]
            a1.append(np.delete(imps_data_dict['realz'],0))
            a2.append(np.delete(imps_data_dict['phasez'],0))
            a3.append(np.delete(imps_data_dict['imagz'],0))
            a4.append(np.delete(imps_data_dict['frequency'],0))
            a5.append(np.delete(imps_data_dict['absz'],0))
            a6.append(np.delete(demod_data_dict['phase'],0))
        plot_big_result(a5,a6,a4)
    else:
        #this is just a set

        try:
            #we delete the first element in the array because it seems to be incorrect almost always
            imps_data_dict = ((list(dic[set]['/dev3481/imps/0/sample']))[0])[0]
            a1 = np.delete(imps_data_dict['realz'],0)
            a2 = np.delete(imps_data_dict['phasez'],0)
            a3 = np.delete(imps_data_dict['imagz'],0)
            a4 = np.delete(imps_data_dict['frequency'],0)
            a5 = np.delete(imps_data_dict['absz'],0)
            plot_result(a1,a5,a4)
        except KeyError:
            succes = False
    return


def rawData_to_arrays():
    path = get_path_through_window()
    print(path)
    flag = False
    dic = load_obj(path)
    try:
        set= int(input("Object has " + str(len(dic)) + " data sets " + "please enter the nimber of the set you want to display or enter 'a' to display all data:"))
    except ValueError:
        flag = True
        print("not a number")

    if(flag):
        #this is all data
        a1 = list()
        a2 = list()
        a3 = list()
        a4 = list()
        a5 = list()
        for key in dic:
            #we delete the first element in the array because it seems to be incorrect almost always
            imps_data_dict = ((list(dic[key]['/dev3481/imps/0/sample']))[0])[0]
            a1.append(np.delete(imps_data_dict['realz'],0))
            a2.append(np.delete(imps_data_dict['phasez'],0))
            a3.append(np.delete(imps_data_dict['imagz'],0))
            a4.append(np.delete(imps_data_dict['frequency'],0))
            a5.append(np.delete(imps_data_dict['absz'],0))
        return (a1,a2,a3,a5,a4)
    else:
        #this is just a set
        try:
            #we delete the first element in the array because it seems to be incorrect almost always
            imps_data_dict = ((list(dic[set]['/dev3481/imps/0/sample']))[0])[0]
            a1 = np.delete(imps_data_dict['realz'],0)
            a2 = np.delete(imps_data_dict['phasez'],0)
            a3 = np.delete(imps_data_dict['imagz'],0)
            a4 = np.delete(imps_data_dict['frequency'],0)
            a5 = np.delete(imps_data_dict['absz'],0)
            return (a1,a2,a3,a5,a4)
        except KeyError:
            succes = False
    return False


rawData_to_plot()
