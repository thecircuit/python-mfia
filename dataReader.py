import _pickle as pickle
import numpy as np
import matplotlib.pyplot as plt


def save_obj(obj, name ):
    with open('obj/'+ name + '.txt', 'wb+') as f:
        pickle.dump(obj, f, 3)

def load_obj(name ):
    with open('obj/'+ name + '.txt','rb') as f:
        return pickle.load(f)

def plot_result(RealZ, ImagZ, frequency):
    """This function plots the results in 2 plots, the real and imaginary
    impedance, against the frequency. The graph will be autoranged on the
    X and Y axis"""
    _, (ax1, ax2) = plt.subplots(2, 1)
    ax2.plot(frequency, ImagZ)
    ax1.plot(frequency, RealZ)
    ax1.grid(True)
    ax1.set_ylabel('R')
    ax1.autoscale()

    ax2.grid(True)
    ax2.set_xlabel('Frequency ($Hz$)')
    ax2.set_ylabel('ImagZ ohm')
    ax2.autoscale()

    plt.draw()
    plt.show()
    return

def datakeys(data,a):
    #data object is a dictionary
    imps_data_dict = ((list(data[a]['/dev3481/imps/0/sample']))[0])[0]
    print("Printing data keys of data of imps object!")
    for key in imps_data_dict:
        print(key)
    imps_data_dict = ((list(data[a]['/dev3481/demods/0/sample']))[0])[0]
    print("Printing data keys of data of demods object!")
    for key in imps_data_dict:
        print(key)
    print("finished!")


def dataread(data,a):
    succes = True
    #data object is a dictionary
    try:
        imps_data_dict = ((list(data[a]['/dev3481/imps/0/sample']))[0])[0]
        absz = imps_data_dict['absz']
        freq = imps_data_dict['frequency']
        phase = imps_data_dict['phasez']
    except KeyError:
        succes = False
        absz = np.empty(shape=0)
        phase = np.empty(shape=0)
        freq = np.empty(shape=0)
    return (absz, phase, freq, succes)
def datareaddemod(data,a):
    succes = True
    #data object is a dictionary
    try:
        imps_data_dict = ((list(data[a]['/dev3481/demods/0/sample']))[0])[0]
        phase = imps_data_dict['phase']
        freq = imps_data_dict['frequency']
        absz = np.empty(shape=(len(freq)))
        for i in range(0,len(freq)):
            absz[i] = 0
    except KeyError:
        succes = False
        phase = np.empty(shape=0)
        freq = np.empty(shape=0)
    return (absz,phase, freq, succes)

#for j in range(0, len(dic[0])):
 #   # Transfer the data from the dictionary to the numpy arrays
    #frequency[j] = int(str(dic[0][j]))
