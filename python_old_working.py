#dit script werkt maar de sweep moet verbeterd worden

import zhinst.ziPython as zi
from scipy.integrate import odeint
import numpy as np
import time
import matplotlib.pyplot as plt
import zhinst.utils
import zhinst.examples
import _pickle as pickle
from collections import deque
import os
import dataReader as reader
import json
import threading
import timer
import datetime
from analysis import Analysis
from collections import defaultdict


def plot_result(RealZ, ImagZ, frequency, center):
    """This function plots the results in 3 plots, the real and imaginary
    impedance, against the frequency. The graph will be autoranged on the
    X and Y axis"""
    _, (ax1, ax2, ax3) = plt.subplots(3, 1)
    for key in range(0, len(frequency)):
        ax2.plot(frequency[key], ImagZ[key])
        ax1.plot(frequency[key], RealZ[key])
        ax3.plot(range(0, len(frequency)), center)
    ax1.grid(True)
    ax1.set_ylabel('absz')
    ax1.autoscale()
    ax3.grid(True)
    ax3.set_ylabel('Resonance Frequency')
    ax3.autoscale()
    ax3.set_xlabel('Dataset')
    ax2.grid(True)
    ax2.set_xlabel('Frequency ($Hz$)')
    ax2.set_ylabel('Phase rad')
    ax2.autoscale()

    plt.draw()
    plt.show()
    return


def create_file(name, header_string,folder):
    with open(folder + '/'+ name + '.txt', 'w', encoding="utf8") as f:
        f.write("\ntest started at " + str(datetime.datetime.now()))
        f.write("\n"+header_string)


def append_file(obj, name, folder, do_header):
    with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            if do_header:
                for keys2 in obj:
                    f.write(keys2+"\t")
                f.write("\n")
            for keys2 in obj:
                f.write(str(obj[keys2])+"\t")

def append_number_to_file(obj, name, folder):
    with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            f.write(str(obj)+"\t")

def append_file2(obj, name, folder):
    dic = obj
    with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            for keys2 in dic:
                f.write("{ " + keys2 + "\t: " + str(dic[keys2]) + " }\t")



def save_obj_raw(obj, name, folder):
    with open(folder + '/'+ name + '.txt', 'wb+') as f:
        pickle.dump(obj, f, 3)


def init_sweep_1():  #adjust to return settings in dictionary
    #adjust to return settings in dictionary
    global mfia_sweep_set
    global daq
    global path
    global device

    settings_dic = defaultdict()
    # The api session is created te connect to the MFIA and communicate with it
    # to set the settings and read the data
    general_setting = [
        ['/%s/demods/*/enable' % device, 0],
        ['/%s/demods/*/trigger' % device, 0],
        ['/%s/sigouts/*/enables/*' % device, 0],
        ['/%s/scopes/*/enable' % device, 0],
        ['/%s/imps/*/enable' % device, 1],
        ['/%s/imps/*/mode' % device, 1],
        ['/%s/imps/*/model' % device, 0],
        ['/%s/imps/*/impedancerange' % device, 1]
                       ]
    daq.set(general_setting)
    settings_dic["general_settings"] = general_setting

    out_channel = 0
    out_mixer_channel = zhinst.utils.default_output_mixer_channel(props)
    print("out_mixer_channel_is: " + str(out_mixer_channel))
    in_channel = 0
    demod_index = 0
    osc_index = 0
    demod_rate = 10000  # was 10e3
    time_constant = 1
    exp_setting = [
        ['/%s/sigins/%d/ac' % (device, in_channel), 0],
        ['/%s/sigins/%d/range' % (device, in_channel), 2 * amplitude],
        ['/%s/demods/%d/enable' % (device, demod_index), 1],
        ['/%s/demods/%d/rate' % (device, demod_index), demod_rate],
        ['/%s/demods/%d/adcselect' % (device, demod_index), 1],
        ['/%s/demods/%d/order' % (device, demod_index), 6],
        ['/%s/demods/%d/timeconstant' % (device, demod_index), time_constant],
        ['/%s/demods/%d/oscselect' % (device, demod_index), osc_index],
        ['/%s/demods/%d/harmonic' % (device, demod_index), 1],
        ['/%s/demods/%d/trigger' % (device, demod_index), 0],
        ['/%s/sigouts/%d/on' % (device, out_channel), 1],
        ['/%s/sigouts/%d/enables/%d' % (device, out_channel, out_mixer_channel), 1],
        ['/%s/sigouts/%d/range' % (device, out_channel), 1],
        ['/%s/sigouts/%d/amplitudes/%d' % (device, out_channel, out_mixer_channel), amplitude]]
    # Some other device-type dependent configuration may be required. For
    # example, disable the signal inputs `diff` and the signal outputs `add` for
    # HF2 instruments.

    daq.set(exp_setting)
    settings_dic["exp_settings"] = exp_setting
    # Unsubscribe any streaming data.
    daq.sync()

    mfia_sweep_set = daq.sweep()

    #
    mfia_sweep_set.set('sweep/device', device)
    settings_dic["sweep/device"] = device

    #
    mfia_sweep_set.set('sweep/gridnode', 'oscs/%d/freq' % osc_index)
    settings_dic["sweep/gridnode"] = 'oscs/%d/freq' % osc_index
    #
    mfia_sweep_set.set('sweep/bandwidthcontrol', 2)
    settings_dic["sweep/bandwidthcontrol"] = 2
    #
    mfia_sweep_set.set('sweep/scan', 0)
    settings_dic["sweep/scan"] = 0
    #
    mfia_sweep_set.set('sweep/settling/time', 0.001)
    settings_dic["sweep/settling/time"] = 0.001
    #
    mfia_sweep_set.set('sweep/averaging/tc', 1)
    settings_dic["sweep/averaging/tc"] = 1
    #
    mfia_sweep_set.set('sweep/averaging/sample', 1)
    settings_dic["sweep/averaging/sample"] = 1
    #
    mfia_sweep_set.set('sweep/xmapping', 1)
    settings_dic["sweep/xmapping"] = 1
    #
    mfia_sweep_set.set('sweep/order', 4)
    settings_dic["sweep/order"] =4
    #
    mfia_sweep_set.set('sweep/settling/inaccuracy', 10)
    settings_dic["sweep/settling/inaccuracy"] =10
    #
    mfia_sweep_set.set('sweep/averaging/time', 0.00000000)
    settings_dic["sweep/averaging/time"] =0.00000000
    #
    mfia_sweep_set.set('sweep/averaging/tc', 1.00000000)
    settings_dic["sweep/averaging/tc"] = 1
    #
    mfia_sweep_set.set('sweep/averaging/sample', 1.0000000)
    settings_dic["sweep/averaging/sample"] = 1
    #
    mfia_sweep_set.set('sweep/maxbandwidth', 1250.0000000)
    settings_dic["sweep/maxbandwidth"] = 1250
    #
    mfia_sweep_set.set('sweep/omegasuppression', 1.0000000)
    settings_dic["sweep/omegasuppression"] = 1
    #
    mfia_sweep_set.set('sweep/phaseunwrap', 0)
    settings_dic["sweep/phaseunwrap"] = 0
    #
    mfia_sweep_set.set('sweep/sincfilter', 0)
    settings_dic["sweep/sincfilter"] = 0
    #
    mfia_sweep_set.set('sweep/awgcontrol', 0)
    settings_dic["sweep/awgcontrol"] = 0
    #
    mfia_sweep_set.set('sweep/historylength', 2)
    settings_dic["sweep/historylength"] = 2
    #
    mfia_sweep_set.set('sweep/bandwidthoverlap', 1)
    settings_dic["sweep/bandwidthoverlap"] = 1
    #
    mfia_sweep_set.set('sweep/bandwidth', 7812.5000000)
    settings_dic["sweep/bandwidth"] = 7812.5
    #
    mfia_sweep_set.set('sweep/endless', 0)
    settings_dic["sweep/endless"] = 0

    return settings_dic


def init_sweep_2():
    #adjust to return settings in dictionary
    global mfia_sweep_set
    global daq
    global path
    global device

    settings_dic = defaultdict()
    # The api session is created te connect to the MFIA and communicate with it
    # to set the settings and read the data
    general_setting = [
        ['/%s/demods/*/enable' % device, 0],
        ['/%s/demods/*/trigger' % device, 0],
        ['/%s/sigouts/*/enables/*' % device, 0],
        ['/%s/scopes/*/enable' % device, 0],
        ['/%s/imps/*/enable' % device, 1],
        ['/%s/imps/*/mode' % device, 1],
        ['/%s/imps/*/model' % device, 0],
        ['/%s/imps/*/impedancerange' % device, 1]
                       ]
    daq.set(general_setting)
    settings_dic["general_settings"] = general_setting

    out_channel = 0
    out_mixer_channel = zhinst.utils.default_output_mixer_channel(props)
    in_channel = 0
    demod_index = 0
    osc_index = 0
    demod_rate = 10000  # was 10e3
    time_constant = 1
    exp_setting = [
        ['/%s/sigins/%d/ac' % (device, in_channel), 0],
        ['/%s/sigins/%d/range' % (device, in_channel), 2 * amplitude],
        ['/%s/demods/%d/enable' % (device, demod_index), 1],
        ['/%s/demods/%d/rate' % (device, demod_index), demod_rate],
        ['/%s/demods/%d/adcselect' % (device, demod_index), 1],
        ['/%s/demods/%d/order' % (device, demod_index), 6],
        ['/%s/demods/%d/timeconstant' % (device, demod_index), time_constant],
        ['/%s/demods/%d/oscselect' % (device, demod_index), osc_index],
        ['/%s/demods/%d/harmonic' % (device, demod_index), 1],
        ['/%s/demods/%d/trigger' % (device, demod_index), 0],
        ['/%s/sigouts/%d/on' % (device, out_channel), 1],
        ['/%s/sigouts/%d/enables/%d' % (device, out_channel, out_mixer_channel), 1],
        ['/%s/sigouts/%d/range' % (device, out_channel), 1],
        ['/%s/sigouts/%d/amplitudes/%d' % (device, out_channel, out_mixer_channel), amplitude]]
    # Some other device-type dependent configuration may be required. For
    # example, disable the signal inputs `diff` and the signal outputs `add` for
    # HF2 instruments.

    daq.set(exp_setting)
    settings_dic["exp_settings"] = exp_setting
    # Unsubscribe any streaming data.
    daq.sync()

    mfia_sweep_set = daq.sweep()

    #
    mfia_sweep_set.set('sweep/device', device)
    settings_dic["sweep/device"] = device

    #
    mfia_sweep_set.set('sweep/gridnode', 'oscs/%d/freq' % osc_index)
    settings_dic["sweep/gridnode"] = 'oscs/%d/freq' % osc_index
    #
    mfia_sweep_set.set('sweep/bandwidthcontrol', 2)
    settings_dic["sweep/bandwidthcontrol"] = 2
    #
    mfia_sweep_set.set('sweep/scan', 0)
    settings_dic["sweep/scan"] = 0
    #
    mfia_sweep_set.set('sweep/settling/time', 0)
    settings_dic["sweep/settling/time"] = 0
    #
    mfia_sweep_set.set('sweep/averaging/tc', 1)
    settings_dic["sweep/averaging/tc"] = 1
    #
    mfia_sweep_set.set('sweep/averaging/sample', 1)
    settings_dic["sweep/averaging/sample"] = 1
    #
    mfia_sweep_set.set('sweep/xmapping', 1)
    settings_dic["sweep/xmapping"] = 1
    #
    mfia_sweep_set.set('sweep/order', 4)
    settings_dic["sweep/order"] =4
    #
    mfia_sweep_set.set('sweep/settling/inaccuracy', 10)
    settings_dic["sweep/settling/inaccuracy"] =10
    #
    mfia_sweep_set.set('sweep/averaging/time', 0.00000000)
    settings_dic["sweep/averaging/time"] =0.00000000
    #
    mfia_sweep_set.set('sweep/averaging/tc', 1.00000000)
    settings_dic["sweep/averaging/tc"] = 1
    #
    mfia_sweep_set.set('sweep/averaging/sample', 1.0000000)
    settings_dic["sweep/averaging/sample"] = 1
    #
    mfia_sweep_set.set('sweep/maxbandwidth', 1250.0000000)
    settings_dic["sweep/maxbandwidth"] = 1250
    #
    mfia_sweep_set.set('sweep/omegasuppression', 1.0000000)
    settings_dic["sweep/omegasuppression"] = 1
    #
    mfia_sweep_set.set('sweep/phaseunwrap', 0)
    settings_dic["sweep/phaseunwrap"] = 0
    #
    mfia_sweep_set.set('sweep/sincfilter', 0)
    settings_dic["sweep/sincfilter"] = 0
    #
    mfia_sweep_set.set('sweep/awgcontrol', 0)
    settings_dic["sweep/awgcontrol"] = 0
    #
    mfia_sweep_set.set('sweep/historylength', 2)
    settings_dic["sweep/historylength"] = 2
    #
    mfia_sweep_set.set('sweep/bandwidthoverlap', 1)
    settings_dic["sweep/bandwidthoverlap"] = 1
    #
    mfia_sweep_set.set('sweep/bandwidth', 7812.5000000)
    settings_dic["sweep/bandwidth"] = 7812.5
    #
    mfia_sweep_set.set('sweep/endless', 0)
    settings_dic["sweep/endless"] = 0

    return settings_dic


def shutdown_mfia():
    global mfia_sweep_set
    global daq
    # Clear the subscription and turn the measurement off

    mfia_sweep_set.unsubscribe(path)
    mfia_sweep_set.unsubscribe('*')
    mfia_sweep_set.unsubscribe(path2)
    # Stop the sweeper thread and clear the memory.
    daq.set([['/%s/imps/*/enable' % device, 0]])
    return


def start_impedance_sweep(start_freq_loc, stop_freq_loc, samples_loc):
    global mfia_sweep_set
    global daq
    global collector_max
    global dataset_collector
    mfia_sweep_set.set('sweep/start', start_freq_loc)
    mfia_sweep_set.set('sweep/stop', stop_freq_loc)
    mfia_sweep_set.set('sweep/samplecount', samples_loc)
    daq.sync()
    mfia_sweep_set.read()
    mfia_sweep_set.subscribe(path)
    mfia_sweep_set.subscribe(path2)
    mfia_sweep_set.execute()
    start = time.time()
    timeout = 180  # [s]
    # print("Will perform", loops, "sweeps...")
    while not mfia_sweep_set.finished():  # Wait until the sweep is complete, with timeout.
        time.sleep(0.5)
        progress = mfia_sweep_set.progress()
        # print("Individual sweep progress: {:.2%}.".format(progress[0]), end="\r")
        print("|{:.0%}".format(progress[0]), end="\r")

        '''if (time.time() - start) > timeout:
            # If for some reason the sweep is blocking, force the end of the
            #measurement.
            print("\nSweep still not finished, forcing finish...")
            mfia_sweep_set.finish()'''
    print("")
    data = mfia_sweep_set.read(True)
    # Stop the sweeper and close the session to the data server.
    mfia_sweep_set.finish()
    mfia_sweep_set.clear()
    return data


def invert_array(array_in):
    array_out = np.empty(shape=(len(array_in)))
    for i in range(0, len(array_in)):
        array_out[i] = (array_in[i]*-1)
    return array_out


def validate_folder(pathName):
    # if folder doesn't exist creates folder
    newpath = pathName
    if not os.path.exists(newpath):
        os.makedirs(newpath)


def mov_avarage(deque_res_freq, in_freq, path4):
    d = deque(deque_res_freq)
    max_samples = 25
    if len(d) > max_samples:
        d.popleft()
        d.append(in_freq)
        append_file2(deque_res_freq, "Mov_avg")
    else:
        d.append(in_freq)

    moving_avarage_file(d, path)
    return d


def moving_avarage_file(deque_res_freq,  folder_path):
    validate_folder(folder_path)


def quick_res_scan():
    test_flag = False
    while False == test_flag:
        collector_max = 0
        #dataholder vars
        dataset_collector = defaultdict(dict)

        # Initial values in hertz for the start_impedance_sweep
        start_freq2 = 141550
        stop_freq2 = 141800
        samples2 = 800
        sweeps_per_excecution2 = 1
        # initialize the mfia
        init_sweep_1()
        # clears the data already stored in read

        mfia_sweep_set.read(True)

        # start the sweep store restults in dataset collector
        print("auto_startfreq: " + str(start_freq2))
        print("auto_stop_freq: " + str(stop_freq2))
        dataset_collector[collector_max] = start_impedance_sweep(start_freq2, stop_freq2, samples2)
        collector_max += 1
        # process data print it to console and save it to file
        (absz2, phase2, freq2, suc2) = reader.dataread(dataset_collector, collector_max-1)
        if suc2:
            phase2 = np.delete(phase2,0)
            peak_finder2 = Analysis()
            freq2 = np.delete(freq2, 0)
            res_frequency2 = freq2[peak_finder2.simple_peak_find(phase2, samples2-1)]
            # Print the resonance frequency
            test_flag = True
    return res_frequency2


"""The main loop initializes the MFIA, subscribes to the impedance sample node
and calls the start_impedance_sweep function with the initial values defined
in the code"""


# debug options
show_timers = True
show_messages = True
amplitude = 0.1
do_plot = True
deq_num = deque()
mov_avg_size = 25

# global vars
err_msg = "This example only supports instruments with demodulators."
out_channel = 0
in_channel = 0
demod_index = 0
osc_index = 0
demod_rate = 10e3
time_constant = 0.01
# The name of the MFIA
device_id = 'dev3481'
#device = 'dev3481'
path = '/dev3481/imps/0/sample'
path2 = '/dev3481/demods/0/sample'


# The apilevel of this program
apilevel_MFIA = 6

collector_max = 0
#dataholder vars
dataset_collector = defaultdict(dict)
moving_average = list()
# The impedance channel that is used(LabOne counts starting at 1, the cade at 0)
imp_index = 0
# locale vars
collector_index = 0
# Initial values in hertz for the start_impedance_sweep
export = dict()
global_timer = timer.Timer()
global_timer.start()

total_measurements = 300
resonance_offset = 18
samples = 150
amount_of_sweeps = 30
fileName = "data_sweep_results"
folderName = ("dataFolder" + str(datetime.datetime.now().strftime("%Y%m%d%H%M%S")))
validate_folder(folderName)

create_file(fileName, "this is peakfinding the phase",folderName)
first = True
measurement_scan = 0

#difine peakfinder
peak_finder = Analysis()


for scans in range(0, total_measurements):
    #necesarry to reboot once in a while else memory errors will occure


    if(scans % 5 == 0):
        (daq, device, props) = zhinst.utils.create_api_session(device_id, apilevel_MFIA, required_devtype='.*IA',required_err_msg=err_msg)

    # do a large bandwith sweep to get an indication where the resonance frequancy is
    print("begin auto ranging")
    begin_freq = quick_res_scan()
    print("auto range done, expected resonance freq = " + str(begin_freq))
    start_freq = begin_freq-resonance_offset
    stop_freq = begin_freq+resonance_offset

    # initialize the mfia for the quick sweeps and save settings to dic
    used_settings = init_sweep_2()
    used_settings["start_freq"] = start_freq
    used_settings["stop_freq"] = stop_freq
    used_settings["samples"] = samples
    used_settings["amount_of_sweeps"] = amount_of_sweeps

    # save settings to file
    create_file("settings", "settings of scan " + str(measurement_scan), folderName)
    append_file2(used_settings, "settings", folderName)
    # clears the data already stored in read
    data = mfia_sweep_set.read(True)


    # Stop the sweeper and close the session to the data server.
    #mfia_sweep_set.finish()
    #mfia_sweep_set.clear()

    # start sweep setup
    timer_measurement = timer.Timer()
    timer_measurement.start()

    # dataholder vars
    dataset_collector = defaultdict(dict)
    collector_max = 0

    # create list for storing data
    absz_list = list()
    phase_list = list()
    freq_list = list()
    res_freq_list = list()

    # start sweeping
    for index in range(0, amount_of_sweeps):
        timer_sweep = timer.Timer()
        timer_sweep.start()
        # start the sweep store restults in dataset collector
        dataset_collector[collector_max] = start_impedance_sweep(start_freq, stop_freq, samples)
        collector_max += 1
        print("time milliseconds passed this sweep : " + str(timer_sweep.passed_time()))
        print("time milliseconds since begin: " + str(timer_measurement.passed_time()))
        # process data print it to console and save it to file
        print(collector_max)
        (absz, phase, freq, succes) = reader.dataread(dataset_collector, collector_max-1)
        if succes:
            # deleting the first element in the array because it seems to be almost always incorrect
            absz = invert_array(np.delete(absz, 0))
            phase = np.delete(phase, 0)
            freq = np.delete(freq, 0)
            # for index in range(0,len(absz)):
            # y_four[index] += phase[index]
            y_three = freq
            absz_list.append(absz)
            phase_list.append(phase)
            freq_list.append(freq)
            # reader.datakeys(dataset_collector)
            # optional for saving file
            # save_obj(dataset_collector,"data")
            #   -1 on samples because we delete one element in the array
            res_frequency = freq[peak_finder.simple_peak_find(phase, samples-1)]
            deq_num.append(res_frequency)
            if len(deq_num) >= mov_avg_size:
                deq_num.popleft()
                append_number_to_file(sum(deq_num)/len(deq_num),"mov_avg",folderName)
            res_freq_list.append(res_frequency)
            # Print the resonance frequency
            print("%.2f hz " % res_frequency)
        else:
            collector_max -= 1
            index -= 1

        # save dataset for testing purpose
        validate_folder("testdat")
        create_file("test", "", "testdat")
        save_obj_raw(dataset_collector, "test", "testdat")

    save_obj_raw(dataset_collector, "rawpikleData", folderName)

    # processing data of all sweeps
    avg_freq = 0
    max_freq = 0
    low_freq = 20000000
    # peak_finder2 = Analysis()
    # print(len(y_three))
    # print(len(y_four))
    # res_frequency2 = y_three[peak_finder2.simple_peak_find(y_four,len(y_three))]
    for i in range(0, len(res_freq_list)):
        avg_freq = avg_freq + res_freq_list[i]
        if res_freq_list[i] > max_freq:
            max_freq = res_freq_list[i]
        if res_freq_list[i] < low_freq:
            low_freq = res_freq_list[i]

    avg_freq = avg_freq /len(res_freq_list)

    combined_phase = phase_list[0]
    for ind2 in range(1,len(phase_list)):
        for ind in range(0,len(phase_list[0])):
            combined_phase[ind] = np.add(combined_phase[ind], phase_list[ind2][ind])
    peakf2 = Analysis()
    res2_frequency = freq_list[0][peakf2.simple_peak_find(combined_phase, samples-1)]

    scandic = dict()
    scandic["MeasurementNumber"] = measurement_scan
    scandic["time"] = str(datetime.datetime.now().strftime("%H:%M:%S"))
    scandic["avarage_freq_pf_sum_avg"] = "%.3f" % avg_freq
    scandic["avarage_freq_comb_pf_avg"] = "%.3f" % res2_frequency
    # scandic["avarage_freq_sum_pf"] = "%.3f" % res_frequency2
    scandic["lowest_freq"] = "%.3f" % low_freq
    scandic["highest_freq"] = "%.3f" % max_freq
    scandic["star_freq"] = "%.3f" % start_freq
    scandic["stop_freq"] = "%.3f" % stop_freq
    scandic["res_freq_std"] = "%.3f" % np.std(np.array(res_freq_list))
    scandic["Delta_resonatie_freq_noise"] = "%.3f" % (max_freq-low_freq)
    scandic["amount_of_sweeps"] = amount_of_sweeps
    scandic["bandwith_sweep"] = str(stop_freq-start_freq)
    scandic["samples_per_sweep"] = samples
    scandic["resolution_hz_sweep"] = "%.3f" % ((stop_freq-start_freq)/(samples-1))
    scandic["total_time_measurement_ms"] = "%.2f" % timer_measurement.passed_time()
    scandic["total_time_multi_measurement_ms"] = "%.2f" % global_timer.save_time_and_report()
    export[str(scans)+"set"] = scandic
    print("the avrage freq = " + str(avg_freq) + "\nThe lowest freq = " + str(low_freq) + "\nthe highest freq = " + str(max_freq) + "\nDelta (noise) freq = " +str(max_freq-low_freq))
    print("scan " + str(measurement_scan+1) + " of " + str(total_measurements ) + "; time so far in sec : " + str(global_timer.saved_time()/1000))
    frequency = []
    append_file(scandic, fileName, folderName, first)
    measurement_scan += 1
    if first:
        first = False

shutdown_mfia()
print("done")
"""end main loop"""
