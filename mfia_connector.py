"""
This runs the MFIA impidance analyzer and creates txt and json files contaning the sweep data
"""
import zhinst
import Mfia_file_manager as file_manager
import analysis
import numpy as np
import time
import matplotlib.pyplot as plt
import zhinst.utils
import zhinst.examples
import _pickle as pickle
from collections import deque
import os
import timer
import datetime
from analysis import Analysis
from collections import defaultdict

__author__ = "Mark Nibourg"
__copyright__ = "Copyright 2018-2019"
__license__ = "GPLv2"
__version__ = "0.9.1"
__maintainer__ = "Mark Nibourg"
__status__ = "Prototype"

class MFIARunner:

    '''
    peak finders should implement the following methodes
    .peakfind(numpyarrayX,numpyarrayY) returns the highest peak in double
    .name() returns the name of the peakfind methode
    '''

    PF_SIMPLE = "PF_SIMPLE"

    PHASE = "PHASE"
    ABSZ = "ABSZ"

    peak_finder = analysis.Analysis()

    def __init__(self, path_set_file):

        # load the file manager and get all the settings
        self.fm = file_manager.MFIA_file_manager("results.json", path_set_file)
        self.general_settings = self.fm.general_settings
        self.init_sweep_settings = self.fm.init_sweep_set
        self.fast_sweep_settings = self.fm.fast_sweep_set

        # first start the connection
        (self.daq, self.device, self.props) = \
            zhinst.utils.create_api_session(
                self.general_settings['device_id']
                , 6
                , required_devtype='.*IA'
                , required_err_msg="Something unexpected went wrong, I don't know what but something broke mate")

        # create sweeper module
        self.sweep = self.daq.sweep()
        self.subscribe_path_module_modulator = self.general_settings['subscribe_path_demod']
        self.subscribe_path_module_mfia = self.general_settings['subscribe_path_imps']

        # set if you want to look for peak in phase or absz
        self.data_type = self.general_settings['datatype']

        # if debug is true the code will report on results inbetween sweeps
        self.debug = self.general_settings['debug_mode']
        # if save_raw is true the raw data from sweeps will be saved
        self.save_raw = self.general_settings['save_raw']

        # make a choice on what peakfinder gets used
        self.peak_finder = self.peak_find_method(self.general_settings['peak_find_algo'])

        # save settings of the project to json
        if self.general_settings["export_data_to_Json"] == 1:
            self.fm.json_object_append_dic("general_settings", self.general_settings)
            self.fm.json_object_append_dic("init_sweep", self.init_sweep_settings)
            self.fm.json_object_append_dic("fast_sweep", self.fast_sweep_settings)
            self.fm.json_object_save()

        # used for saving measurement results
        self.measurement_dic = defaultdict()
        self.sweep_dic = defaultdict()
        self.res_freq_list = list()

    def set_settings_sweeper(self, sweep_settings):
        # this should be a broad sweep to get a estimation of where the resonace freq is located
        # takes a path to a json file with settings
        daq_set = sweep_settings["general_settings"]
        sweep_set = sweep_settings["mfia_settings"]


        for key in daq_set:
            s = [[key,daq_set[key]]]
            #print("key is: " + str(key) + " value is : " + str(daq_set[key]) + " value type is : " + str(type(daq_set[key])))
            self.daq.set(s)
        for key in sweep_set:
            self.sweep.set(key, sweep_set[key])
            #print("key is: " + str(key) + " value is : " +str(sweep_set[key]) + " value type is : " + str(type(sweep_set[key])))

    @staticmethod
    def __mode(self, mode):
        # declare in what mode the measurement gets used
        switcher = {
            0: self.__start_mov_avg_scanning_amount(),
            1: self.__start_mov_avg_scanning_timed(),
            2: self.__start_sett_scanning_amount(),
            3: self.__start_sett_scanning_timed()
        }
        return switcher.get(mode)

    def start_impedance_sweep(self,start_freq_loc, stop_freq_loc, samples_loc):
        self.sweep.set('sweep/start', start_freq_loc)
        self.sweep.set('sweep/stop', stop_freq_loc)
        self.sweep.set('sweep/samplecount', samples_loc)
        self.daq.sync()
        self.sweep.read()
        self.sweep.subscribe(self.subscribe_path_module_mfia)
        self.sweep.subscribe(self.subscribe_path_module_modulator)
        self.sweep.execute() # Error is thrown here
        start = time.time()
        timeout = 180  # [s]
        # print("Will perform", loops, "sweeps...")
        while not self.sweep.finished():  # Wait until the sweep is complete, with timeout.
            time.sleep(0.5)
            progress = self.sweep.progress()
            # print("Individual sweep progress: {:.2%}.".format(progress[0]), end="\r")
            print("|{:.0%}".format(progress[0]), end="\r")

            '''if (time.time() - start) > timeout:
                # If for some reason the sweep is blocking, force the end of the
                #measurement.
                print("\nSweep still not finished, forcing finish...")
                mfia_sweep_set.finish()'''
        print("")
        return self.sweep.read(True)


    def quick_res_scan(self):
        test_flag = False
        while False == test_flag:
            collector_max2 = 0
            #dataholder vars
            dataset_collector2 = defaultdict(dict)

            sweeps_per_excecution2 = 1
            # initialize the mfia
            self.set_settings_sweeper(self.init_sweep_settings)

            # clears the data already stored in read
            start_freq = self.general_settings["start_freq_init_sweep"]
            stop_freq = self.general_settings["stop_freq_init_sweep"]
            samples = self.general_settings["samples_init_sweep"]
            self.sweep.read(True)

            # start the sweep store restults in dataset collector
            if self.debug:
                print("auto_startfreq: " + str(start_freq))
                print("auto_stop_freq: " + str(stop_freq))
                print("auto_samples: " + str(samples))
            dataset_collector2[collector_max2] = self.start_impedance_sweep(start_freq, stop_freq, samples)
            collector_max2 += 1
            # process data print it to console and save it to file
            (absz2, phase2, freq2, suc2) = self.fm.data_read(dataset_collector2, collector_max2-1)

            if suc2:
                if self.data_type == self.ABSZ:
                    print("1")
                    absz2 = self.invert_array(np.delete(absz2, 0))
                    freq2 = np.delete(freq2, 0)
                    res_frequency2 = freq2[self.peak_finder.simple_peak_find(absz2, samples-1)]
                    # Print the resonance frequency
                    test_flag = True
                if self.data_type ==self.PHASE:
                    print("2")
                    phase2 = np.delete(phase2,0)
                    freq2 = np.delete(freq2, 0)
                    res_frequency2 = freq2[self.peak_finder.simple_peak_find(phase2, samples-1)]
                    # Print the resonance frequency
                    test_flag = True
        print(res_frequency2)
        return res_frequency2

    def shutdown_mfia(self):
        # Clear the subscription and turn the measurement off

        self.sweep.unsubscribe(self.general_settings["subscribe_path_imps"])
        self.sweep.unsubscribe('*')
        self.sweep.unsubscribe(self.general_settings["subscribe_path_demod"])

        return


    @staticmethod
    def invert_array(array_in):
        array_out = np.empty(shape=(len(array_in)))
        for i in range(0, len(array_in)):
            array_out[i] = (array_in[i] * -1)
        return array_out

    def start_mov_avg_scanning_amount(self, amount_scans, mov_avg_size):
        print()

    def start_mov_avg_scanning_timed(self,mov_avg_size):
        # debug options
        deq_num = deque()
        export = dict()
        big_timer = timer.Timer()
        big_timer.start()
        first = True
        begin_freq = 0
        measurement_scan = 0
        #difine peakfinder
        #peak_finder = Analysis()
        for scans in range(0, self.general_settings["time_in_seconds"]):
            #reset measurementresults
            self.measurement_dic = defaultdict()

            if first:
                # do a large bandwith sweep to get an indication where the resonance frequancy is
                print("begin auto ranging")
                begin_freq = self.quick_res_scan()
            print("auto range done, expected resonance freq = " + str(begin_freq))
            start_freq = begin_freq - self.general_settings["offset_center_frequency"]
            stop_freq = begin_freq + self.general_settings["offset_center_frequency"]

            # clears the data already stored in read
            data = self.sweep.read(True)

            # start timers for timing the measurement
            timer_measurement = timer.Timer()
            timer_measurement.start()

            avg_time_sweep = 0
            avg_time_calc_sweep = 0

            # clear dataholder vars
            dataset_collector = defaultdict(dict)
            collector_max = 0

            # create list for storing data
            res_freq_list = list()
            self.set_settings_sweeper(self.fast_sweep_settings)

            # get peakfinder
            peak_finder = self.peak_find_method(self.general_settings["peak_find_algo"])

            # start sweeping
            for index in range(0, self.general_settings["amount_of_sweep_per_measurement"]):

                # reset sweep results
                self.sweep_dic = defaultdict()
                # start timer for measurering calculation time sweep
                timer_calc_sweep = timer.Timer()

                # start timer for single sweep
                timer_sweep = timer.Timer()
                timer_sweep.start()




                # start the sweep, store results in dataset collector
                dataset_collector[collector_max] = self.start_impedance_sweep(start_freq, stop_freq, self.general_settings["samples_normal_sweep"])

                # stop timer sweep start calc timer
                timer_sweep.pause()

                timer_calc_sweep.start()

                # increment counter
                collector_max += 1

                # print related information too screen if debug mode is on
                if self.debug == 1:
                    print("time milliseconds passed this sweep : " + str(timer_sweep.passed_time()))
                    print("time milliseconds since begin: " + str(timer_measurement.passed_time()))

                # parse data from data set to lists
                (absz, phase, freq, succes) = self.fm.data_read(dataset_collector, collector_max-1)

                if succes:
                    # deleting the first element in the array because it seems to be almost always incorrect
                    freq = np.delete(freq, 0)

                    #   -1 on samples because we delete one element in the array
                    # todo change peakfinder methode
                    if self.data_type == self.PHASE:
                        phase = np.delete(phase, 0)
                        res_frequency = freq[peak_finder.simple_peak_find(phase, self.general_settings["samples_normal_sweep"]-1)]
                    elif self.data_type == self.ABSZ:
                        absz = self.invert_array(np.delete(absz, 0))
                        res_frequency = freq[peak_finder.simple_peak_find(absz, self.general_settings["samples_normal_sweep"]-1)]
                    else:
                        raise ValueError("Datatype " + str(self.PHASE) + " specified in settings is not compatible")

                    # append the found resonance freq to data set
                    res_freq_list.append(res_frequency)
                    # Print the resonance frequency

                    print("%.2f hz " % res_frequency)
                else:
                    collector_max -= 1
                    index -= 1



                self.sweep_dic["resonance_freq"] = res_frequency
                self.sweep_dic["phase"] = absz.tolist()
                self.sweep_dic["absz"] = absz.tolist()
                self.sweep_dic["freq"] = freq.tolist()
                self.sweep_dic["timer_sweep"] = timer_sweep.passed_time()
                self.sweep_dic["timer_measurement"] = timer_measurement.passed_time()
                self.measurement_dic["resonance_freq"] = self.res_freq_list
                self.measurement_dic["sweep_result_" + str(index)] = self.sweep_dic

                # timer processing
                avg_time_sweep += timer_sweep.passed_time()
                avg_time_calc_sweep += timer_calc_sweep.passed_time()

            if self.general_settings["save_raw"] == 1:
                folder_name = "rawdata"
                self.fm.validate_folder(folder_name)
                self.fm.save_obj_raw(dataset_collector, "rawpikleData", folder_name)

            # processing data of all sweeps
            avg_freq = 0
            max_freq = 0
            low_freq = 20000000

            # search for highest, lowest and average resonance freq value
            for i in range(0, len(res_freq_list)):
                avg_freq = avg_freq + res_freq_list[i]
                if res_freq_list[i] > max_freq:
                    max_freq = res_freq_list[i]
                if res_freq_list[i] < low_freq:
                    low_freq = res_freq_list[i]

            avg_freq /= len(res_freq_list)
            begin_freq = avg_freq

            # calculate the avarage sweep time
            avg_time_sweep = avg_time_sweep/self.general_settings["amount_of_sweep_per_measurement"]
            avg_time_calc_sweep = avg_time_calc_sweep/self.general_settings["amount_of_sweep_per_measurement"]


            scandic = dict()
            scandic["MeasurementNumber"] = measurement_scan
            scandic["time"] = str(datetime.datetime.now().strftime("%H:%M:%S"))
            scandic["avarage_freq_pf_sum_avg"] = "%.3f" % avg_freq
            scandic["lowest_freq"] = "%.3f" % low_freq
            scandic["highest_freq"] = "%.3f" % max_freq
            scandic["star_freq"] = "%.3f" % start_freq
            scandic["stop_freq"] = "%.3f" % stop_freq
            scandic["res_freq_std"] = "%.3f" % np.std(np.array(res_freq_list))
            scandic["Delta_resonatie_freq_noise"] = "%.3f" % (max_freq-low_freq)
            scandic["amount_of_sweeps"] = self.general_settings["amount_of_sweep_per_measurement"]
            scandic["bandwith_sweep"] = str(stop_freq-start_freq)
            scandic["samples_per_sweep"] = self.general_settings["samples_normal_sweep"]
            scandic["resolution_hz_sweep"] = "%.3f" % ((stop_freq-start_freq)/(self.general_settings["samples_normal_sweep"]-1))
            scandic["avg_time_calc_ms"] = "%.2f" % avg_time_calc_sweep
            scandic["avg_time_sweep_ms"] = "%.2f" % avg_time_sweep
            scandic["total_time_measurement_ms"] = "%.2f" % timer_measurement.passed_time()
            scandic["total_time_multi_measurement_ms"] = "%.2f" % big_timer.save_time_and_report()
            export[str(scans)+"set"] = scandic
            print("the avrage freq = " + str(avg_freq) + "\nThe lowest freq = " + str(low_freq) + "\nthe highest freq = " + str(max_freq) + "\nDelta (noise) freq = " +str(max_freq-low_freq))
            print("scan " + str(measurement_scan+1) + " of " + str(self.general_settings["amount_of_measurements"] ) + "; time so far in sec : " + str(big_timer.saved_time()/1000))

            # save results
            self.measurement_dic["measurement_results"] = scandic
            if self.general_settings["export_data_to_Json"] == 1:
                self.fm.json_object_append_dic("measurement" + str(scans), self.measurement_dic)
                self.fm.json_object_save()

            if self.general_settings["save_results_to_text"] == 1:
                self.fm.text_append_results_dic(scandic)

            measurement_scan += 1
            if first:
                first = False


        self.shutdown_mfia()
        print("done")
        """end main loop"""

    def start_group_avg_amount(self):

        # debug options
        deq_num = deque()
        export = dict()
        big_timer = timer.Timer()
        big_timer.start()
        first = True
        begin_freq = 0
        measurement_scan = 0
        #difine peakfinder
        #peak_finder = Analysis()
        for scans in range(0, self.general_settings["amount_of_measurements"]):
            #reset measurementresults
            self.measurement_dic = defaultdict()

            if first:
                # do a large bandwith sweep to get an indication where the resonance frequancy is
                print("begin auto ranging")
                begin_freq = self.quick_res_scan()
            print("auto range done, expected resonance freq = " + str(begin_freq))
            start_freq = begin_freq - self.general_settings["offset_center_frequency"]
            stop_freq = begin_freq + self.general_settings["offset_center_frequency"]

            # clears the data already stored in read
            data = self.sweep.read(True)

            # start timers for timing the measurement
            timer_measurement = timer.Timer()
            timer_measurement.start()

            avg_time_sweep = 0
            avg_time_calc_sweep = 0

            # clear dataholder vars
            dataset_collector = defaultdict(dict)
            collector_max = 0

            # create list for storing data
            absz_list = list()
            phase_list = list()
            freq_list = list()
            res_freq_list = list()
            self.set_settings_sweeper(self.fast_sweep_settings)

            # get peakfinder
            peak_finder = self.peak_find_method(self.general_settings["peak_find_algo"])

            # start sweeping
            for index in range(0, self.general_settings["amount_of_sweep_per_measurement"]):

                # reset sweep results
                self.sweep_dic = defaultdict()
                # start timer for measurering calculation time sweep
                timer_calc_sweep = timer.Timer()

                # start timer for single sweep
                timer_sweep = timer.Timer()
                timer_sweep.start()




                # start the sweep, store results in dataset collector
                dataset_collector[collector_max] = self.start_impedance_sweep(start_freq, stop_freq, self.general_settings["samples_normal_sweep"])

                # stop timer sweep start calc timer
                timer_sweep.pause()

                timer_calc_sweep.start()

                # increment counter
                collector_max += 1

                # print related information too screen if debug mode is on
                if self.debug == 1:
                    print("time milliseconds passed this sweep : " + str(timer_sweep.passed_time()))
                    print("time milliseconds since begin: " + str(timer_measurement.passed_time()))

                # parse data from data set to lists
                (absz, phase, freq, succes) = self.fm.data_read(dataset_collector, collector_max-1)

                if succes:
                    # deleting the first element in the array because it seems to be almost always incorrect
                    freq = np.delete(freq, 0)

                    #   -1 on samples because we delete one element in the array
                    # todo change peakfinder methode
                    if self.data_type == self.PHASE:
                        phase = np.delete(phase, 0)
                        res_frequency = freq[peak_finder.simple_peak_find(phase, self.general_settings["samples_normal_sweep"]-1)]
                    elif self.data_type == self.ABSZ:
                        absz = self.invert_array(np.delete(absz, 0))
                        res_frequency = freq[peak_finder.simple_peak_find(absz, self.general_settings["samples_normal_sweep"]-1)]
                    else:
                        raise ValueError("Datatype " + str(self.PHASE) + " specified in settings is not compatible")

                    # append the found resonance freq to data set
                    res_freq_list.append(res_frequency)
                    # Print the resonance frequency

                    print("%.2f hz " % res_frequency)
                else:
                    collector_max -= 1
                    index -= 1



                self.sweep_dic["resonance_freq"] = res_frequency
                self.sweep_dic["phase"] = absz.tolist()
                self.sweep_dic["absz"] = absz.tolist()
                self.sweep_dic["freq"] = freq.tolist()
                self.sweep_dic["timer_sweep"] = timer_sweep.passed_time()
                self.sweep_dic["timer_measurement"] = timer_measurement.passed_time()
                self.measurement_dic["resonance_freq"] = self.res_freq_list
                self.measurement_dic["sweep_result_" + str(index)] = self.sweep_dic

                # timer processing
                avg_time_sweep += timer_sweep.passed_time()
                avg_time_calc_sweep += timer_calc_sweep.passed_time()

            if self.general_settings["save_raw"] == 1:
                folder_name = "rawdata"
                self.fm.validate_folder(folder_name)
                self.fm.save_obj_raw(dataset_collector, "rawpikleData", folder_name)

            # processing data of all sweeps
            avg_freq = 0
            max_freq = 0
            low_freq = 20000000

            # search for highest, lowest and average resonance freq value
            for i in range(0, len(res_freq_list)):
                avg_freq = avg_freq + res_freq_list[i]
                if res_freq_list[i] > max_freq:
                    max_freq = res_freq_list[i]
                if res_freq_list[i] < low_freq:
                    low_freq = res_freq_list[i]

            avg_freq /= len(res_freq_list)
            begin_freq = avg_freq

            # calculate the avarage sweep time
            avg_time_sweep = avg_time_sweep/self.general_settings["amount_of_sweep_per_measurement"]
            avg_time_calc_sweep = avg_time_calc_sweep/self.general_settings["amount_of_sweep_per_measurement"]


            scandic = dict()
            scandic["MeasurementNumber"] = measurement_scan
            scandic["time"] = str(datetime.datetime.now().strftime("%H:%M:%S"))
            scandic["avarage_freq_pf_sum_avg"] = "%.3f" % avg_freq
            scandic["lowest_freq"] = "%.3f" % low_freq
            scandic["highest_freq"] = "%.3f" % max_freq
            scandic["star_freq"] = "%.3f" % start_freq
            scandic["stop_freq"] = "%.3f" % stop_freq
            scandic["res_freq_std"] = "%.3f" % np.std(np.array(res_freq_list))
            scandic["Delta_resonatie_freq_noise"] = "%.3f" % (max_freq-low_freq)
            scandic["amount_of_sweeps"] = self.general_settings["amount_of_sweep_per_measurement"]
            scandic["bandwith_sweep"] = str(stop_freq-start_freq)
            scandic["samples_per_sweep"] = self.general_settings["samples_normal_sweep"]
            scandic["resolution_hz_sweep"] = "%.3f" % ((stop_freq-start_freq)/(self.general_settings["samples_normal_sweep"]-1))
            scandic["avg_time_calc_ms"] = "%.2f" % avg_time_calc_sweep
            scandic["avg_time_sweep_ms"] = "%.2f" % avg_time_sweep
            scandic["total_time_measurement_ms"] = "%.2f" % timer_measurement.passed_time()
            scandic["total_time_multi_measurement_ms"] = "%.2f" % big_timer.save_time_and_report()
            export[str(scans)+"set"] = scandic
            print("the avrage freq = " + str(avg_freq) + "\nThe lowest freq = " + str(low_freq) + "\nthe highest freq = " + str(max_freq) + "\nDelta (noise) freq = " +str(max_freq-low_freq))
            print("scan " + str(measurement_scan+1) + " of " + str(self.general_settings["amount_of_measurements"] ) + "; time so far in sec : " + str(big_timer.saved_time()/1000))

            # save results
            self.measurement_dic["measurement_results"] = scandic
            if self.general_settings["export_data_to_Json"] == 1:
                self.fm.json_object_append_dic("measurement" + str(scans), self.measurement_dic)
                self.fm.json_object_save()

            if self.general_settings["save_results_to_text"] == 1:
                self.fm.text_append_results_dic(scandic)

            measurement_scan += 1
            if first:
                first = False


        self.shutdown_mfia()
        print("done")
        """end main loop"""

    def __start_group_avg_timed(self):
        print()

    def __start_auto_range_scan(self):
        print()


    @staticmethod
    def peak_find_method(mode):
        # this selects the right method
        switcher = {
            "PF_SIMPLE": analysis.Analysis(),
        }
        return switcher.get(mode)


