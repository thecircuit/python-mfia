"""
This reads and writes data from the mfia impedance analyser
"""

import _pickle as pickle
import datetime
import _pickle as pickle
import datetime
import json
import os
import time
from collections import defaultdict
from collections import deque

import matplotlib.pyplot as plt
import numpy as np
import zhinst.examples
import zhinst.utils

import timer
from analysis import Analysis

import matplotlib.pyplot as plt

__author__ = "Mark Nibourg"
__copyright__ = "Copyright 2018-2019"
__license__ = "GPLv2"
__version__ = "0.9.1"
__maintainer__ = "Mark Nibourg"
__status__ = "Prototype"


class MFIA_file_manager:

    def __init__(self, path_save_results, path_file_settings):
        # # first we get the settings from the json settings file
        self.json_exist = False
        self.text_exist = False
        self.json_object = defaultdict()
        # generate dicts
        self.general_settings = defaultdict()
        self.fast_sweep_set = defaultdict()
        self.init_sweep_set = defaultdict()

        # open json file where we stored our settings
        with open(path_file_settings) as x:
            data = json.load(x)

        # transfer data from json to internal dicts for later use
        for key in data['basic_settings']:
            self.general_settings[key] = data['basic_settings'][key]

        for key in data['init_sweep_set']:
            self.fast_sweep_set[key] = defaultdict()
            for key2 in data['init_sweep_set'][key]:
                self.fast_sweep_set[key][key2] = data['init_sweep_set'][key][key2]

        for key in data['fast_sweep_set']:
            self.init_sweep_set[key] = defaultdict()
            for key2 in data['fast_sweep_set'][key]:
                self.init_sweep_set[key][key2] = data['fast_sweep_set'][key][key2]

        # save files to the following folder
        self.save_directory = path_save_results

        # create sub folder for storing data in
        self.path_folder = str(datetime.datetime.now().strftime("%Y-%m-%d_%Hi%Mi%S")) + "_results"
        self.validate_folder(self.path_folder)

        # create path for file that's going to store result data
        self.name_results_json_file = self.path_folder + '/results.json'
        self.name_results_text_file = self.path_folder + '/results.txt'
        # self.__json_create_file(self.name_results_file, "this is a test header")

        self.name_results_sweep_raw = 'results_raw.txt'

    def json_object_clear(self):
        self.json_object = defaultdict

    def json_object_append_dic(self, name, dic):
        self.json_object[name] = dic


    def json_object_save(self):
        with open(self.name_results_json_file, 'w') as f:
            json.dump(self.json_object, f, ensure_ascii=False, indent=1, separators=(',', ':'))

    def json_object_load(self):
        with open(self.name_results_json_file, 'r') as f:
            return json.load(f)

    def text_append_results_dic(self, data):
        if not self.text_exist:
            self.__text_create_results_dic(data)
            self.text_exist = True
        with open(self.name_results_text_file, 'a') as f:
            f.write("\n")
            for keys2 in data:
                f.write(str(data[keys2])+"\t")

    def __text_create_results_dic(self, data):
        # creates the file with the header data
        with open(self.name_results_text_file, 'w') as f:
            f.write("\n")
            for keys2 in data:
                f.write(str(keys2)+"\t")

    def get_init_sweep_settings(self):
        return self.init_sweep_set

    def get_fast_sweep_settings(self):
        return self.fast_sweep_set

    def get_settings(self):
        return self.general_settings

    def raw_dat_to_file(self):
        print()
        #saves raw numpy array data from mfia with pickle

    def mfia_create_results_file(self,path,name_file,settings,comment):
        print()
        #first line should have the name of the test and time when it was started
        #second part should have the settings used in the measurement
        #third line holds the comment for the file
        # fourth line holds keys names of the to be saved information
        #creates the file where the results gets stored,

    def mfia_append_to_results_file(self,data_results):
        print()
        # saves data to txt file with '\t' in between data for easy import in excel

    def mfia_sweep_settings_parser(self, path):
        #reads a json file and returns the data in a dict

        with open(path) as f:
            data = json.load(f)
        set1 = defaultdict()
        set2 = defaultdict()

        for key in data[0]:
            set1[key] = data[0][key]
        for key in data[1]:
            set2[key] = data[1][key]

        return set1, set2


    def mfia_create_mov_avg_file(self,path,name_file,comment):
        print()

    def mfia_append_to_mov_avg_file(self,path,data):
        print()

    def mfia_results_text_to_json(self,path_file,path_json):
        print()

    def mfia_mov_avg_tect_to_json(self,path_file,path_json):
        print()

#--------------------helper functions

    def __plot_result(self,RealZ, ImagZ, frequency, center):
        """This function plots the results in 3 plots, the real and imaginary
        impedance, against the frequency. The graph will be autoranged on the
        X and Y axis"""
        _, (ax1, ax2, ax3) = plt.subplots(3, 1)
        for key in range(0, len(frequency)):
            ax2.plot(frequency[key], ImagZ[key])
            ax1.plot(frequency[key], RealZ[key])
            ax3.plot(range(0, len(frequency)), center)
        ax1.grid(True)
        ax1.set_ylabel('absz')
        ax1.autoscale()
        ax3.grid(True)
        ax3.set_ylabel('Resonance Frequency')
        ax3.autoscale()
        ax3.set_xlabel('Dataset')
        ax2.grid(True)
        ax2.set_xlabel('Frequency ($Hz$)')
        ax2.set_ylabel('Phase rad')
        ax2.autoscale()

        plt.draw()
        plt.show()
        return

    @staticmethod
    def data_read(data, a):
        succes = True
        #data object is a dictionary
        try:
            imps_data_dict = ((list(data[a]['/dev3481/imps/0/sample']))[0])[0]
            absz = imps_data_dict['absz']
            freq = imps_data_dict['frequency']
            phase = imps_data_dict['phasez']
        except KeyError:
            succes = False
            absz = np.empty(shape=0)
            phase = np.empty(shape=0)
            freq = np.empty(shape=0)
        return absz, phase, freq, succes

    def plot_result(self,RealZ, ImagZ, frequency, center):
        """This function plots the results in 3 plots, the real and imaginary
        impedance, against the frequency. The graph will be autoranged on the
        X and Y axis"""
        _, (ax1, ax2, ax3) = plt.subplots(3, 1)
        for key in range(0, len(frequency)):
            ax2.plot(frequency[key], ImagZ[key])
            ax1.plot(frequency[key], RealZ[key])
            ax3.plot(range(0, len(frequency)), center)
        ax1.grid(True)
        ax1.set_ylabel('absz')
        ax1.autoscale()
        ax3.grid(True)
        ax3.set_ylabel('Resonance Frequency')
        ax3.autoscale()
        ax3.set_xlabel('Dataset')
        ax2.grid(True)
        ax2.set_xlabel('Frequency ($Hz$)')
        ax2.set_ylabel('Phase rad')
        ax2.autoscale()

        plt.draw()
        plt.show()
        return

    @staticmethod
    def __append_file(obj, name, folder, do_header):
        with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            if do_header:
                for keys2 in obj:
                    f.write(keys2 + "\t")
                f.write("\n")
            for keys2 in obj:
                f.write(str(obj[keys2]) + "\t")

    @staticmethod
    def __append_number_to_file(obj, name, folder):
        with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            f.write(str(obj) + "\t")

    @staticmethod
    def __append_file2(obj, name, folder):
        dic = obj
        with open(folder + '/' + name + '.txt', 'a', encoding="utf8") as f:
            f.write("\n")
            for keys2 in dic:
                f.write("{ " + keys2 + "\t: " + str(dic[keys2]) + " }\t")

    @staticmethod
    def save_obj_raw(obj, name, folder):
        with open(folder + '/' + name + '.txt', 'wb+') as f:
            pickle.dump(obj, f, 3)

    @staticmethod
    def validate_folder(path_name):
        # if folder doesn't exist creates folder
        if not os.path.exists(path_name):
            os.makedirs(path_name)
