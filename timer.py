import time


class Timer:

    def __init__(self):
        self.begin = 0
        self.save = 0
        self.passed = 0
        self.paused = True

    def start(self):
        if self.paused:
            self.begin = time.clock()
            self.paused = False
        else:
            raise ValueError('Couldn\'t start timer because it has already started!')

    def pause(self):
        if not self.paused:
            self.passed = (time.clock()-self.begin)
            self.paused = True
        else:
            raise ValueError('Couldn\'t pause timer because it has already been paused!')

    def passed_time(self):
        return (((time.clock()-self.begin)+self.passed)*1000)

    def save_time_and_report(self):
        if not self.paused:
            self.save = (((time.clock()-self.begin)+self.passed)*1000)
            return self.save
        else:
            return self.passed*1000

    def saved_time(self):
        return self.save

    def report(self):
        if not self.paused:
            print("time passed is " + str(((time.clock()-self.begin)+self.passed)*1000) + " milliseconds!")
        else:
            print("time passed is " + str((self.passed)*1000) + " milliseconds!")